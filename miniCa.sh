#! /usr/bin/env bash

docker run --rm -ti \
    -v $(pwd):$(pwd) \
    -w $(pwd) \
    michalhosna/minica:master "$@"
    