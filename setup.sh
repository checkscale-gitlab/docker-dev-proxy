#! /usr/bin/env bash
set -euo pipefail
IFS=$'\n\t'

PROJECT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

set -x

mkdir -p $PROJECT_DIR/ca
cd $PROJECT_DIR/ca

$PROJECT_DIR/miniCa.sh -ca-cert ca-cert.pem -ca-key ca-key.pem -domains gravel

cd $PROJECT_DIR

# Assure docker is running
docker info > /dev/null

docker network create proxy || true

# docker-compose up -d